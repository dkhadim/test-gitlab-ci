#!/bin/bash

function setup_env () {

    if [ $# -eq 1 ]
    then
        echo "Using the current in $1"
    else
        echo "Using app for other server"
    fi
}

function setup {

    echo "Running config"
}

function deploy {

    echo "Deployment in server started ..."
    setup
    echo "Running and test setup env"
    echo "=========== 1 ============"
    setup_env dev
    echo "=========== 2 ============"
    setup_env
}